//
//  AdsUnity.m
//  Ads
//
//  Created by Ünal Zubari on 9/25/16.
//  Copyright (c) 2013 Gideros Mobile. All rights reserved.
//
#include "gideros.h"
#import "AdsUnity.h"
#import "AdsClass.h"

@implementation AdsUnity
-(id)init{

    self.mngr = [[AdsManager alloc] init];
    return self;
}

-(void)destroy{
    [self.mngr destroy];
    [self.mngr release];
    self.mngr = nil;
}

-(void)setKey:(NSMutableArray*)parameters{
   // [UnityAds setDebugMode:false];
    [UnityAds initialize:[parameters objectAtIndex:0] delegate:self testMode:false];
}

-(void)loadAd:(NSMutableArray*)parameters{
    NSString *type = [parameters objectAtIndex:0];
  
if ([type isEqualToString:@"video"] || [type isEqualToString:@"defaultZone"] || [type isEqualToString:@"defaultVideoAndPictureZone"]) {
    self.placementId = @"video";
        AdsStateChangeListener *listener = [[AdsStateChangeListener alloc] init];
        [listener setShow:^(){
            [AdsClass adDisplayed:[self class] forType:type];
                    [UnityAds show:[AdsClass getRootViewController] placementId:@"video"];
        }];
        [listener setDestroy:^(){}];
        [listener setHide:^(){}];
        [self.mngr set:listener forType:type withListener:listener];
       
        //if([Chartboost hasInterstitial:tag]){
            [self.mngr load:type];
            [AdsClass adReceived:[self class] forType:type];
        //}
        //else
            //[Chartboost cacheInterstitial:tag];
    }
   else if ([type isEqualToString:@"rewardedVideo"] || [type isEqualToString:@"rewardedVideoZone"] || [type isEqualToString:@"incentivizedZone"] || [type isEqualToString:@"v4vc"]  )  {
        self.placementId = @"v4vc";
        AdsStateChangeListener *listener = [[AdsStateChangeListener alloc] init];
        [listener setShow:^(){
            [AdsClass adDisplayed:[self class] forType:type];
            [UnityAds show:[AdsClass getRootViewController] placementId:@"rewardedVideo"];
        }];
        [listener setDestroy:^(){}];
        [listener setHide:^(){}];
        [self.mngr set:listener forType:type withListener:listener];
        
        //if([Chartboost hasRewardedVideo:tag]){
            [self.mngr load:type];
            [AdsClass adReceived:[self class] forType:type];
        //}
        //else
           // [Chartboost cacheRewardedVideo:tag];
    }
    else
    {
         [AdsClass adError:[self class] with:[NSString stringWithFormat:@"Unknown type: %@", type]];
    }
}

-(void)showAd:(NSMutableArray*)parameters{
    NSString *type = [parameters objectAtIndex:0];
    if([self.mngr get:type] == nil)
        [self loadAd:parameters];
    [self.mngr show:type];
}

-(void)hideAd:(NSString*)type{
    [self.mngr hide:type];
}

-(void)enableTesting{
}

-(UIView*)getView{
    return nil;
}


//UnityAdsDelegate interface implementation

- (void)unityAdsReady:(NSString *)placementId {
    NSLog(@"UADS Ready");

    [AdsClass adReceived:[self class] forType:placementId ];
    [self.mngr load:placementId];
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    
    NSLog(@"UnityAds ERROR: %ld - %@",(long)error, message);
    [AdsClass adFailed:[self class] with:@"ERROR" forType:self.placementId ];
    [self.mngr reset:self.placementId];
    
}

- (void)unityAdsDidStart:(NSString *)placementId {
    NSLog(@"UADS Start");
    [AdsClass adActionBegin:[self class] forType: placementId];

}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    NSString *stateString = @"UNKNOWN";
    switch (state) {
        case kUnityAdsFinishStateError:
            stateString = @"ERROR";
            [AdsClass adFailed:[self class] with:@"ERROR" forType: placementId ];
            [self.mngr reset:placementId];
            break;
        case kUnityAdsFinishStateSkipped:
            stateString = @"SKIPPED";
            [AdsClass adDismissed:[self class] forType: placementId ]; 
            break;
        case kUnityAdsFinishStateCompleted:
            [AdsClass adActionEnd:[self class] forType: placementId ];
            stateString = @"COMPLETED";
            break;
        default:
            break;
    }
    NSLog(@"UnityAds FINISH: %@ - %@", stateString, placementId);
}


@end

