//
//  AdsAdmob.h
//  Ads
//
//  Created by Ünal Zubari on 9/25/16.
//  Copyright (c) 2013 Gideros Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdsProtocol.h"
#import "AdsManager.h"
#import <UnityAds/UnityAds.h>

@interface AdsUnity : NSObject <AdsProtocol, UnityAdsDelegate >
@property (nonatomic, retain) AdsManager *mngr;
@property (nonatomic, retain) NSString *placementId;
@end
