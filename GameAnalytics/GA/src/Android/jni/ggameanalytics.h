#ifndef GGAMEANALYTICS_H
#define GGAMEANALYTICS_H

#include <gglobal.h>

#ifdef __cplusplus
extern "C" {
#endif

void ggameanalytics_initializeWithGameKey( const char * gamekey, const char * secretkey );
void ggameanalytics_configureBuild( const char * build);
    
#ifdef __cplusplus
}
#endif

#endif
