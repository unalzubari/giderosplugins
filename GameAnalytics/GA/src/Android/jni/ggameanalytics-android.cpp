#include "ggameanalytics.h"
#include <jni.h>
#include <stdlib.h>

extern "C" {
JavaVM *g_getJavaVM();
JNIEnv *g_getJNIEnv();
}

static jobject parametersToMap(const char **parameters)
{
    if (parameters == NULL)
        return NULL;

	JNIEnv *env = g_getJNIEnv();
	
	jclass cls = env->FindClass("java/util/HashMap");	

	jobject jmapobj = env->NewObject(cls, env->GetMethodID(cls, "<init>", "()V"));
	
	while (*parameters && *(parameters + 1))
    {
		jstring jKey = env->NewStringUTF(*parameters);
		jstring jVal = env->NewStringUTF(*(parameters + 1));
		env->CallObjectMethod(jmapobj, env->GetMethodID(cls, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"), jKey, jVal);
		env->DeleteLocalRef(jKey);
		env->DeleteLocalRef(jVal);	
		parameters += 2;
	}

	env->DeleteLocalRef(cls);

	return jmapobj;	
}

extern "C" {

void ggameanalytics_initializeWithGameKey( const char * gamekey, const char * secretkey )
{
	JNIEnv *env = g_getJNIEnv();

	jclass cls = env->FindClass("com/giderosmobile/android/plugins/gameanalytics/GGameAnalytics");	
	jstring jgameKey   = env->NewStringUTF(gamekey);
	jstring jsecretKey = env->NewStringUTF(secretkey);
	env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "initializeWithGameKey", "(Ljava/lang/String;Ljava/lang/String;)V"), jgameKey,jsecretKey);
	env->DeleteLocalRef(jgameKey);
	env->DeleteLocalRef(jsecretKey);
	env->DeleteLocalRef(cls);
}


void ggameanalytics_configureBuild( const char * build){

	JNIEnv *env = g_getJNIEnv();

	jclass cls = env->FindClass("com/giderosmobile/android/plugins/gameanalytics/GGameAnalytics");	
	jstring jbuild   = env->NewStringUTF(build);
	env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "configureBuild", "(Ljava/lang/String;)V"), jbuild );
	env->DeleteLocalRef(jbuild);
	env->DeleteLocalRef(cls);
}
/*
void gflurry_LogEvent(const char *eventName, const char **parameters, int timed)
{
	JNIEnv *env = g_getJNIEnv();

	jclass cls = env->FindClass("com/giderosmobile/android/plugins/flurry/GFlurry");	

    jstring eventName2 = env->NewStringUTF(eventName);

    jobject parameters2 = parametersToMap(parameters);
    
    if (parameters2 && timed)
		env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "logEvent", "(Ljava/lang/String;Ljava/util/Map;Z)V"), eventName2, parameters2, (jboolean)1);
    else if (!parameters2 && timed)
		env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "logEvent", "(Ljava/lang/String;Z)V"), eventName2, (jboolean)1);
    else if (parameters2 && !timed)
		env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "logEvent", "(Ljava/lang/String;Ljava/util/Map;)V"), eventName2, parameters2);
    else if (!parameters2 && !timed)
		env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "logEvent", "(Ljava/lang/String;)V"), eventName2);

	env->DeleteLocalRef(eventName2);
	
	if (parameters2)
		env->DeleteLocalRef(parameters2);

	env->DeleteLocalRef(cls);
}

void gflurry_EndTimedEvent(const char *eventName, const char **parameters)
{
	JNIEnv *env = g_getJNIEnv();

	jclass cls = env->FindClass("com/giderosmobile/android/plugins/flurry/GFlurry");	

    jstring eventName2 = env->NewStringUTF(eventName);

    jobject parameters2 = parametersToMap(parameters);

	if (parameters2)
		env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "endTimedEvent", "(Ljava/lang/String;Ljava/util/Map;)V"), eventName2, parameters2);
	else
		env->CallStaticVoidMethod(cls, env->GetStaticMethodID(cls, "endTimedEvent", "(Ljava/lang/String;)V"), eventName2);
	
	env->DeleteLocalRef(eventName2);
	
	if (parameters2)
		env->DeleteLocalRef(parameters2);

	env->DeleteLocalRef(cls);
}*/

}
