#include "ggameanalytics.h"
#include "gideros.h"
#include <vector>
#include <string>

#ifndef abs_index
#define abs_index(L, i) ((i) > 0 || (i) <= LUA_REGISTRYINDEX ? (i) : lua_gettop(L) + (i) + 1)
#endif

static int isSessionStarted = 0;

static char **copyParameters(lua_State *L, int index)
{
	luaL_checktype(L, index, LUA_TTABLE);

	int t = abs_index(L, index);
    
    std::vector<std::string> parameters;
	lua_pushnil(L);
	while (lua_next(L, t) != 0)
	{
        parameters.push_back(luaL_checkstring(L, -2));
        parameters.push_back(luaL_checkstring(L, -1));
		lua_pop(L, 1);
    }
    
    char **parameters2 = (char**)malloc((parameters.size() + 1) * sizeof(char*));
    for (std::size_t i = 0; i < parameters.size(); ++i)
        parameters2[i] = strdup(parameters[i].c_str());
    parameters2[parameters.size()] = NULL;

    return parameters2;
}

static void freeParameters(char **parameters)
{
    if (parameters == NULL)
        return;
    
    char **parameters2 = parameters;
    
    while (*parameters2)
        free(*parameters2++);

    free(parameters);
}

/*
static int logEvent(lua_State *L)
{
    const char *eventName = luaL_checkstring(L, 1);
    
    char **parameters = NULL;
    if (!lua_isnoneornil(L, 2))
        parameters = copyParameters(L, 2);
    
    int timed = lua_toboolean(L, 3);
    
    gflurry_LogEvent(eventName, (const char **)parameters, timed);
    
    freeParameters(parameters);
    
    return 0;
}

static int endTimedEvent(lua_State *L)
{
    const char *eventName = luaL_checkstring(L, 1);
    
    char **parameters = NULL;
    if (!lua_isnoneornil(L, 2))
        parameters = copyParameters(L, 2);

    gflurry_EndTimedEvent(eventName, (const char **)parameters);
    
    freeParameters(parameters);
    
    return 0;
}
*/
static int initializeWithGameKey(lua_State *L ){
    const char *gamekey = luaL_checkstring(L, 1);
    const char *secretkey = luaL_checkstring(L, 2);

    ggameanalytics_initializeWithGameKey( gamekey, secretkey );

    return 0;

}

static int configureBuild( lua_State *L ){

    const char *build = luaL_checkstring(L, 1);
    ggameanalytics_configureBuild( build );

    return 0;
}



static int loader(lua_State* L)
{
	lua_newtable(L);
    
	lua_pushcfunction(L, initializeWithGameKey);
	lua_setfield(L, -2, "initializeWithGameKey");
	lua_pushcfunction(L, configureBuild);
	lua_setfield(L, -2, "configureBuild");
	
/*
    lua_pushcfunction(L, configureAvailableResourceCurrencies);
	lua_setfield(L, -2, "configureAvailableResourceCurrencies");
	lua_pushcfunction(L, configureAvailableResourceItemTypes);
	lua_setfield(L, -2, "configureAvailableResourceItemTypes");
    lua_pushcfunction(L, resourceEvent);
    lua_setfield(L, -2, "resourceEvent");

    lua_pushcfunction(L, progressionEvent);
    lua_setfield(L, -2, "progressionEvent");

    lua_pushcfunction(L, errorEvent);
    lua_setfield(L, -2, "errorEvent");

    lua_pushcfunction(L, addDesignEventWithEventId);
    lua_setfield(L, -2, "addDesignEventWithEventId");

*/
    /*
    setCustomDimension01
    setCustomDimension02
    setCustomDimension03
    setUserInfo
    configureAvailableCustomDimensions01
    configureAvailableCustomDimensions02
    configureAvailableCustomDimensions03
    */



	lua_pushvalue(L, -1);
	lua_setglobal(L, "gameanalytics");
    
	return 1;
}

static void g_initializePlugin(lua_State *L)
{
	lua_getglobal(L, "package");
	lua_getfield(L, -1, "preload");
    
	lua_pushcfunction(L, loader);
	lua_setfield(L, -2, "gameanalytics");
    
	lua_pop(L, 2);
}

static void g_deinitializePlugin(lua_State *L)
{
}

REGISTER_PLUGIN("GameAnalytics", "1.0")

