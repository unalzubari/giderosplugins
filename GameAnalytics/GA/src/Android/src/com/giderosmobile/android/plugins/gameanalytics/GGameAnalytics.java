package com.giderosmobile.android.plugins.gameanalytics;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Vector;

import android.app.Activity;

import com.gameanalytics.sdk.*;

public class GGameAnalytics
{
	private static WeakReference<Activity> sActivity;
	//private static volatile boolean sIsActive = true;
	//private static volatile boolean isInitiated = false;
	private static String sApiKey = null;

	public static void onCreate(Activity activity)
	{
		sActivity = new WeakReference<Activity>(activity);

		GAPlatform.initializeWithActivity( sActivity.get() );
		// Enable log
        GameAnalytics.setEnabledInfoLog(false);
        GameAnalytics.setEnabledVerboseLog(false);

		//FlurryAgent.setLogEnabled(false);
	}

	public static void configureAvailableResourceCurrencies( Vector< String > vCurrencies ){
	    
	    //vCurrencies.size() <=20

	    //Each resource currency string should only contain [A-Za-z] characters.

	     StringVector currencies = new StringVector();
        currencies.add("gems");
        currencies.add("gold");
        GameAnalytics.configureAvailableResourceCurrencies(currencies);
	}

	public statc void configureAvailableResourceItemTypes( Vector< String > viTemTypes){

		//viTemTypes.size()<=20
		
		StringVector itemTypes = new StringVector();
        itemTypes.add("boost");
        itemTypes.add("lives");
        GameAnalytics.configureAvailableResourceItemTypes(itemTypes);
	}

	public static void configureAvailableCustomDimensions01( Vector< String > vDim ){
		
		//vDim.size() <=20
		        // Configure available custom dimensions
        StringVector customDimension01 = new StringVector();
        customDimension01.add("ninja");
        customDimension01.add("samurai");
        GameAnalytics.configureAvailableCustomDimensions01(customDimension01);
	}

	public static void configureAvailableCustomDimensions02( Vector< String > vDim ){
		
		//vDim.size() <=20
		 // Configure available custom dimensions
        StringVector customDimension02 = new StringVector();
        customDimension02.add("whale");
        customDimension02.add("dolphin");
        GameAnalytics.configureAvailableCustomDimensions02(customDimension02);
	}
	public static void configureAvailableCustomDimensions03( Vector< String > vDim ){

		//vDim.size() <=20
		        // Configure available custom dimensions
        StringVector customDimension03 = new StringVector();
        customDimension03.add("horde");
        customDimension03.add("alliance");
        GameAnalytics.configureAvailableCustomDimensions03(customDimension03);
	}

	public static void configureBuild( String build){
		       // Configure build version
	       
	        GameAnalytics.configureBuild(build);
	}

	public static void initializeWithGameKey( String gamekey, String secretkey){
		// Initialize
		
		GameAnalytics.initializeWithGameKey( gamekey, secretkey );
	}


	public static void resourceEvent( Map<String, String> mVal ){
    //https://github.com/GameAnalytics/GA-SDK-ANDROID/wiki/Resource%20Event

	}	

	public static void progressionEvent(){

	   //https://github.com/GameAnalytics/GA-SDK-ANDROID/wiki/Progression%20Event
	}

	public static void errorEvent(){
		//https://github.com/GameAnalytics/GA-SDK-ANDROID/wiki/Error%20Event
		
		//GameAnalytics.addErrorEventWithSeverity(GAErrorSeverity.GAErrorSeverityDebug, "Something went bad in some of the smelly code!");

	}

	public static addDesignEventWithEventId( Map< Stirng, String > mVal ){

		//https://github.com/GameAnalytics/GA-SDK-ANDROID/wiki/Design%20Event
		
		//GameAnalytics.addDesignEventWithEventId("BossFights:FireLord:KillTimeUsed", 234);
	}

	public static setCustomDimension(){
		//https://github.com/GameAnalytics/GA-SDK-ANDROID/wiki/Using%20Custom%20Dimensions
		
		//GameAnalytics.setCustomDimension01("ninja");
		//GameAnalytics.setCustomDimension02("dolphin");
		//GameAnalytics.setCustomDimension03("horde");

	}

	public static setUserInfo( Map < String, String > mVal ){

		// https://github.com/GameAnalytics/GA-SDK-ANDROID/wiki/User%20Information
		
		//GameAnalytics.setGender("female");
		//GameAnalytics.setBirthYear(1980);
		//GameAnalytics.setFacebookId("123456789012345");

	}
	
}
